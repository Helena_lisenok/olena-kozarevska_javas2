const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather{
constructor(){
this.temperature=''
this.wind=''
this.date=Date.now()
}

async setWeather(cityName){
    await axios.get(weatherUrl + cityName).then((response) => 
    {
        
        this.temperature = response.data.temperature
        this.wind = response.data.wind
    }).catch((error) => {return error.message})
}
}
module.exports={Weather}
