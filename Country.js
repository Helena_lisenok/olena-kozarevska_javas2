const { City } = require("./city")

class Country {

    constructor(name) {
        this.name = name
        this.cities = []
    }

    addCity(city) {
        this.cities.push(city)
    }

    delCity(name) {
        const index = this.cities.findIndex(c => c.name === name)
        if (index !== -1) {
            this.cities.splice(index, 1) 
        } else {
            console.warn('City not found')
        }
    }
}
module.exports = { Country }


