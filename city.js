const { Weather } = require('./weather')
class City {
    constructor(name) {
        this.name = name
        this.weather = new Weather()
    }
    async setWeather() {
        await this.weather.setWeather(this.name)
    }
}
module.exports = { City }
